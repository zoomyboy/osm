<?php

namespace Zoomyboy\Osm;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class FillCoordsJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public Geolocatable $model)
    {
        $this->onQueue('forever');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Redis::throttle('osm')->block(0)->allow(1)->every(3)->then(function () {
            $address = $this->model->getAddressForGeolocation();
            if (!$address) {
                $this->model->destroyCoordinate();

                return;
            }
            $coordinate = $address->getCoordinate($this->model::geolocationService());

            if (!$coordinate) {
                return;
            }
            $this->model->fillCoordinate($coordinate);
        }, function () {
            return $this->release(5);
        });
    }
}
