<?php

namespace Zoomyboy\Osm;

trait HasGeolocation
{
    public static bool $geolocationEnabled = true;

    public static function bootHasGeolocation(): void
    {
        static::saved(function (Geolocatable $model) {
            if (!static::$geolocationEnabled) {
                return;
            }

            if ($model->needsGeolocationUpdate()) {
                dispatch(new FillCoordsJob($model));
            }
        });
    }

    public static function disableGeolocation(): void
    {
        static::$geolocationEnabled = false;
    }

    public static function enableGeolocation(): void
    {
        static::$geolocationEnabled = true;
    }
}
