<?php

namespace Zoomyboy\Osm;

interface Geolocatable
{
    public function fillCoordinate(Point $point): void;

    public function getAddressForGeolocation(): ?Address;

    public function destroyCoordinate(): void;

    public function needsGeolocationUpdate(): bool;

    public static function geolocationService(): string;
}
