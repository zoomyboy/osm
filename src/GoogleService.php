<?php

namespace Zoomyboy\Osm;

use Illuminate\Support\Facades\Http;

class GoogleService
{
    public function getAddress(string $query): ?Point
    {
        $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json?' . http_build_query([
            'address' => $query,
            'key' => config('services.osm.google_key'),
        ]));

        if (!$response->ok()) {
            return null;
        }

        return Point::from([
            'lat' => (float) data_get($response, 'results.0.geometry.location.lat'),
            'lon' => (float) data_get($response, 'results.0.geometry.location.lng')
        ]);
    }
}
