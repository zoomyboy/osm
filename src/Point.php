<?php

namespace Zoomyboy\Osm;

use Spatie\LaravelData\Data;

class Point extends Data
{
    public function __construct(public float $lat, public float $lon)
    {
    }

    public function imageUrl(): string
    {
        return '/osm-static-maps/?' . http_build_query([
            'center' => $this->lon . ',' . $this->lat,
            'zoom' => 20,
            'maxZoom' => 13,
        ]);
    }

    public function markerUrl(): string
    {
        return '/osm-static-maps/?' . http_build_query([
            'geojson' => json_encode(['type' => 'Point', 'coordinates' => [$this->lon, $this->lat]]),
            'zoom' => 20,
            'maxZoom' => 20,
        ]);
    }
}
