<?php

namespace Zoomyboy\Osm;


class Address
{
    public function __construct(
        public ?string $address,
        public ?string $zip,
        public ?string $location
    ) {
    }

    public function getCoordinate($serviceClass): ?Point
    {
        if (!$this->address || !$this->zip || !$this->location) {
            return null;
        }

        return app($serviceClass)->getAddress($this->queryString());
    }

    public function queryString(): string
    {
        return $this->address . ', ' . $this->zip . ' ' . $this->location;
    }
}
