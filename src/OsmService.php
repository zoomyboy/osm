<?php

namespace Zoomyboy\Osm;

use Illuminate\Support\Facades\Http;

class OsmService
{
    public function getAddress(string $query): ?Point
    {
        $response = Http::get('https://nominatim.openstreetmap.org/search?' . http_build_query([
            'q' => $query,
            'format' => 'json',
            'addressdetails' => 1,
        ]));

        if (!$response->ok()) {
            return null;
        }

        return count($response->json()) ? Point::from(['lat' => (float) data_get($response, '0.lat'), 'lon' => (float) data_get($response, '0.lon')]) : null;
    }
}
